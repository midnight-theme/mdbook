# Plugin: Admonish

The plugin [mdbook-admonish](https://github.com/tommilligan/mdbook-admonish) adds that adds useful colored boxes.

The CSS file for the midnight theme of the plugin must be added **after** the original plugin CSS.

```toml
{{#include ../book.toml:11}}
```

---

```admonish note
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish abstract
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish summary
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish tldr
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish info
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish todo
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish tip
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish hint
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish important
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish success
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish check
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish done
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish question
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish help
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish faq
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish warning
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish caution
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish attention
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish failure
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish fail
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish missing
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish danger
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish error
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish bug
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish example
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish quote
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```

```admonish cite
Rust is a multi-paradigm, general-purpose programming language designed for performance and safety, especially safe concurrency.
```
