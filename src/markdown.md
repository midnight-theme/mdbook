# Markdown

This page exists to test the markdown styling.

Contents from [the mdBook book](https://rust-lang.github.io/mdBook/format/markdown.html) with some exceptions to fix
them for this book.

## Text and Paragraphs

Text is rendered relatively predictably:

```markdown
Here is a line of text.

This is a new line.
```

Will look like you might expect:

Here is a line of text.

This is a new line.

## Headings

Headings use the `#` marker and should be on a line by themselves. More `#` mean smaller headings:

```markdown
### A heading

Some text.

#### A smaller heading

More text.
```

### A heading

Some text.

#### A smaller heading

More text.

## Lists

Lists can be unordered or ordered. Ordered lists will order automatically:

```markdown
* milk
* eggs
* butter

1. carrots
2. celery
3. radishes
```

* milk
* eggs
* butter

1. carrots
2. celery
3. radishes

## Links

Linking to a URL or local file is easy:

```markdown
Use [mdBook](https://github.com/rust-lang/mdBook).

Read about [mdBook](highlightjs.md).

A bare url: <https://www.rust-lang.org>.
```

Use [mdBook](https://github.com/rust-lang/mdBook).

Read about [mdBook](highlightjs.md).

A bare url: <https://www.rust-lang.org>.

----

Relative links that end with `.md` will be converted to the `.html` extension.
It is recommended to use `.md` links when possible.
This is useful when viewing the Markdown file outside mdBook, for example, on GitHub or GitLab which renders Markdown
automatically.

Links to `README.md` will be converted to `index.html`.
This is done since some services like GitHub render README files automatically, but web servers typically expect the
root file to be called `index.html`.

You can link to individual headings with `#` fragments.
For example, `mdbook.md#text-and-paragraphs` would link to the [Text and Paragraphs](#text-and-paragraphs) section
above.
The ID is created by transforming the heading such as converting to lowercase and replacing spaces with dashes.
You can click on any heading and look at the URL in your browser to see what the fragment looks like.

## Images

Including images is simply a matter of including a link to them, much like in the _Links_ section above. The following
markdown includes the Rust logo SVG image found in the `images` directory at the same level as this file:

```markdown
![The Midnight Theme mdBook Logo](images/midnight-theme-mdbook-logo.svg)
```

Produces the following HTML when built with mdBook:

```html
<p><img src="images/midnight-theme-mdbook-logo.svg" alt="The Midnight Theme mdBook Logo"/></p>
```

Which, of course, displays the image like so:

![The Midnight Theme mdBook Logo](images/midnight-theme-mdbook-logo.svg)

## Extensions

mdBook has several extensions beyond the standard CommonMark specification.

### Strikethrough

Text may be rendered with a horizontal line through the center by wrapping the
text with two tilde characters on each side:

```md
> An example of ~~strikethrough text~~.
```

This example will render as:

> An example of ~~strikethrough text~~.

This follows the [GitHub Strikethrough extension][strikethrough].

### Footnotes

A footnote generates a small numbered link in the text which when clicked
takes the reader to the footnote text at the bottom of the item. The footnote
label is written similarly to a link reference with a caret at the front. The
footnote text is written like a link reference definition, with the text
following the label. Example:

```md
> This is an example of a footnote[^note].
>
> [^note]: This text is the contents of the footnote, which will be rendered
> towards the bottom.
```

This example will render as:

> This is an example of a footnote[^note].
>
> [^note]: This text is the contents of the footnote, which will be rendered
> towards the bottom.

The footnotes are automatically numbered based on the order the footnotes are
written.

### Tables

Tables can be written using pipes and dashes to draw the rows and columns of
the table. These will be translated to HTML table matching the shape. Example:

```md
| Header1 | Header2 |
|---------|---------|
| abc     | def     |
| ghi     | jkl     |
| mno     | pqr     |
```

This example will render similarly to this:

| Header1 | Header2 |
|---------|---------|
| abc     | def     |
| ghi     | jkl     |
| mno     | pqr     |

See the specification for the [GitHub Tables extension][tables] for more
details on the exact syntax supported.

### Task lists

Task lists can be used as a checklist of items that have been completed.
Example:

```md
- [x] Complete task
- [ ] Incomplete task
```

This will render as:

> - [x] Complete task
> - [ ] Incomplete task

See the specification for the [task list extension] for more details.

### Smart punctuation

Some ASCII punctuation sequences will be automatically turned into fancy Unicode
characters:

| ASCII sequence | Unicode                      |
|----------------|------------------------------|
| `--`           | –                            |
| `---`          | —                            |
| `...`          | …                            |
| `"`            | “ or ”, depending on context |
| `'`            | ‘ or ’, depending on context |

So, no need to manually enter those Unicode characters!

This feature is disabled by default.
To enable it, see the [`output.html.curly-quotes`] config option.

<!-- @formatter:off -->
[strikethrough]: https://github.github.com/gfm/#strikethrough-extension-
[tables]: https://github.github.com/gfm/#tables-extension-
[task list extension]: https://github.github.com/gfm/#task-list-items-extension-
[`output.html.curly-quotes`]: configuration/renderers.md#html-renderer-options
<!-- @formatter:off -->
