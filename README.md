# Midnight Theme — mdBook generator

<div align="center">
    <img src="etc/midnight-theme-mdbook-logo.svg" alt="midnight theme mdbook logo" width="300">
</div>

___

This is the midnight theme repository for the [mdBook generator][project-website].

## Colors

The general definition of the colors and specification how to use them can be found in
the [meta project of the midnight theme][meta].

## Installation

To install the theme, simply drop the `midnight` folder contained in the release into the mdBook project and configure
the theme in the `book.toml`.

```toml
[output.html]
theme = "midnight"
default-theme = "midnight"
preferred-dark-theme = "midnight"
```

> **⚠ Warning**
>
> Please be aware that the midnight theme removes all predefined themes.
> To add the midnight theme to the existing ones, it is necessary to manually change the original theme files and only
> copy the required files.

### Plugin: Admonish

The [mdbook-admonish](https://github.com/tommilligan/mdbook-admonish) plugin enables the generation of colorful
admonishments.
This plugin is optional, and so is the CSS file for it.

If the plugin styles are required, they must be downloaded separately from the release page.
To install it, simply add the CSS file
as [`additional-css`](https://rust-lang.github.io/mdBook/format/configuration/renderers.html#html-renderer-options)
in the HTML renderer option **after** the original `mdbook-admonish.css`.

```toml
additional-css = ["./css/mdbook-admonish.css", "./css/midnight-admonish.css"]
```

## Related projects

For code examples, the [highlight.js midnight theme][midnight-theme-highlightjs] is used.
Therefore, a copy was added.
See the [highlight.css](https://gitlab.com/midnight-theme/mdbook/-/blob/main/midnight/highlight.css) file for the verson
information.

Highlight.js itself has been added in version `v11.7.0` with the following languages:

* All in the category "Common"
* .properties
* AsciiDoc
* Handlebars
* Batch file (DOS)
* Dockerfile
* HTTP

## Release

A public release will be created when a git tag gets pushed to GitLab. The changelog must be added manually to that
release.

The release workflow is as follows:

_Note: `X.X.X` must be replaced with the release version._

1. Update the `CHANGELOG.md` using [git-cliff][git-cliff]
    * Command: `git cliff --config .config/cliff.toml --output CHANGELOG.md --tag X.X.X`
2. Add and commit all changes
    * Command: `git add . && git commit -m "chore(release): prepare for version X.X.X"`
3. Create a signed tag
    * Command: `git tag -s X.X.X -m "chore(release): version X.X.X"`
4. Push everything
    * Command: `git push && git push --tags`

<!-- @formatter:off -->
[project-website]: https://rust-lang.github.io/mdBook/
[meta]: https://gitlab.com/midnight-theme/meta/
[midnight-theme-highlightjs]: https://gitlab.com/midnight-theme/highlightjs
[git-cliff]: https://github.com/orhun/git-cliff
<!-- @formatter:on -->

