<!-- @formatter:off -->
# Changelog

All notable changes to this project will be documented in this file.

## [%0.1.0] - 2022-11-24

### 🔨 Miscellaneous Tasks

- [[`a7075906`](https://gitlab.com/michewl/midnight-theme/mdbook/-/commit/a70759063cb324ff5ef8d70f1211e75efc539e4b)] Initial commit

<!-- @formatter:on -->
